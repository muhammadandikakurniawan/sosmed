﻿using Backend.Object.Models;
using Backend.Object.Objects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Service.Interfaces
{
    public interface IAuthService
    {
        public Task<ServiceResultModel<LoginResultObject>> LoginAsync(LoginParamObject param);
        public Task<bool> AuthenticationAsync(string param);
        public Task<ServiceResultModel<DataUserObject>> GetUserByAccessTokenAsync(string token);

        Task<ServiceResultModel<DataUserObject>> RefreshAccessTokenAsync(string token);
    }
}

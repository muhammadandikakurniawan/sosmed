﻿using Backend.ElasticSearchRepository.Entities;
using Backend.Object.Models;
using Backend.Object.Objects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Service.Interfaces
{
    public interface IUserPostService
    {
        public Task<DataPageResultModel<List<UserPost>>> GetAllPagerAsync(int from, int size);
        public Task<DataPageResultModel<List<UserPost>>> GetByUserIDPagerAsync(string userId, int from, int size);
        public Task<ServiceResultModel<UserPost>> AddPost(AddPostParamObject param);
        public Task<ServiceResultModel<UserPost>> AddPost(UserPost param);
        public Task<ServiceResultModel<UserPost>> PushAddPost(AddPostParamObject param);
        public Task<ServiceResultModel<UserPost>> SubcribeAddPost(string message);
        public Task<ServiceResultModel<UserPost>> PushNotifAddPost(ServiceResultModel<UserPost> param);
    }
}

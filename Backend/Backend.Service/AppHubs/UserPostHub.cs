﻿
using Backend.ElasticSearchRepository.Entities;
using Backend.Object.Models;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Service.AppHubs
{
    public class UserPostHub : Hub
    {
        public async Task PushNotifAddPost(ServiceResultModel<UserPost> param)
        {
            await Clients.All.SendAsync("PushNotifAddPost", param);
        }

        internal Task SendAsync(string v, ServiceResultModel<UserPost> param)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using Backend.ElasticSearchRepository.Entities;
using Backend.ElasticSearchRepository.Repositories.Interfaces;
using Backend.Object.Models;
using Backend.Object.Objects;
using Backend.PsgSQLRepository.Entities;
using Backend.PsgSQLRepository.Repositories.Interfaces;
using Backend.Service.Interfaces;
using Backend.Utility.ExtendClass;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Backend.Utility.Utilities;
using Newtonsoft.Json;
using Backend.Service.AppHubs;
using Microsoft.AspNetCore.SignalR;

namespace Backend.Service.Implements
{
    public class UserPostService : IUserPostService
    {
        //repositories
        private IUserRepository _userRepository;
        private IUserPostEsRepository _userPostEsRepository;

        //addition class
        private readonly IHttpContextAccessor _httpContextAccessor;

        private IHubContext<UserPostHub> _userPostHub;

        public UserPostService(
            IUserRepository userRepository
            , IUserPostEsRepository userPostEsRepository
            , IHttpContextAccessor httpContextAccessor
            , IHubContext<UserPostHub> userPostHub
        )
        {
            _userRepository = userRepository;
            _userPostEsRepository = userPostEsRepository;

            _httpContextAccessor = httpContextAccessor;
            _userPostHub = userPostHub;
        }

        public async Task<ServiceResultModel<UserPost>> SubcribeAddPost(string message)
        {
            ServiceResultModel<UserPost> result = new ServiceResultModel<UserPost>();
            result.StatusCode = "00";
            try
            {

                UserPost paramAddPost = JsonConvert.DeserializeObject<UserPost>(message);

                var insertPostRes = _userPostEsRepository.Insert(paramAddPost);
                if (!insertPostRes.IsSuccess)
                {
                    result.StatusCode = "11";
                    result.Message = $"insert data failed : {insertPostRes.Message}";
                    var produceUserPostRes = await KafkaUtil.ProducerKafka(
                        AppSettings.KafkaSettings.BootstrapServers,
                        AppSettings.KafkaSettings.AddUserPostTopic,
                        JsonConvert.SerializeObject(paramAddPost)
                    );
                    return result;
                }

                result.Value = paramAddPost;

            }
            catch (Exception ex)
            {
                string errorMsg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                result.Message = $"Internal System Error : {errorMsg}";
                result.StatusCode = "500";
            }
            return result;
        }

        public async Task<ServiceResultModel<UserPost>> AddPost(UserPost param)
        {
            ServiceResultModel<UserPost> result = new ServiceResultModel<UserPost>();
            result.StatusCode = "00";
            try
            {

                var insertPostRes = _userPostEsRepository.Insert(param);
                if (!insertPostRes.IsSuccess)
                {
                    result.StatusCode = "11";
                    result.Message = $"insert data failed : {insertPostRes.Message}";
                    return result;
                }

                result.Value = param;

            }
            catch(Exception ex)
            {
                string errorMsg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                result.Message = $"Internal System Error : {errorMsg}";
                result.StatusCode = "500";
            }
            return result;
        }

        public async Task<ServiceResultModel<UserPost>> AddPost(AddPostParamObject param)
        {
            ServiceResultModel<UserPost> result = new ServiceResultModel<UserPost>();
            result.StatusCode = "00";
            try
            {

                User dataUser = _userRepository.GetById(param.UserId);

                if (dataUser == null)
                {
                    result.StatusCode = "10";
                    result.Message = "user not found";
                    return result;
                }
                if (dataUser.Isdeleted)
                {
                    result.StatusCode = "10";
                    result.Message = "user not found";
                    return result;
                }

                UserPost dataPost = new UserPost();
                dataPost.UseId = dataUser.Id;
                //dataPost.Title = param.Title;
                dataPost.Description = param.Description;

                if (param.Files != null && param.Files.Count > 0)
                {
                    string[] imageTypes = new string[] { };
                    string[] videoTypes = new string[] { };
                    int fileCount = 0;
                    dataPost.Files = param.Files.Select(f => {

                        UserPostFile file = new UserPostFile();

                        file.Extension = f.FileName.Split(".").Last();
                        file.FileName = $"{dataUser.Id}_{Guid.NewGuid().ToString()}_{DateTime.Now.Ticks.ToString()}_{fileCount}.{file.Extension}";
                        file.IsVideo = f.ContentType.ToLower().Contains("video");
                        file.IsImage = f.ContentType.ToLower().Contains("image");
                        file.Extension = f.FileName.Split(".").Last();
                        file.Size = f.Length;

                        string filePath = Path.Combine(AppSettings.UserPostFilesDir, file.FileName);
                        
                        using (Stream fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            if (file.IsImage)
                            {
                                ExtendUtil.saveAndResizeImage(fileStream, 1000, 800, filePath);
                            }
                            else
                            {
                                f.CopyTo(fileStream);
                            }
                        }

                        fileCount++;
                        return file;
                    }).ToList();
                }

                var insertPostRes = _userPostEsRepository.Insert(dataPost);
                if (!insertPostRes.IsSuccess)
                {
                    result.StatusCode = "11";
                    result.Message = $"insert data failed : {insertPostRes.Message}";
                    return result;
                }

                result.Value = dataPost;

            }
            catch (Exception ex)
            {
                string errorMsg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                result.Message = $"Internal System Error : {errorMsg}";
                result.StatusCode = "500";
            }
            return result;
        }

        public async Task<ServiceResultModel<UserPost>> PushAddPost(AddPostParamObject param)
        {
            ServiceResultModel<UserPost> result = new ServiceResultModel<UserPost>();
            result.StatusCode = "00";
            try
            {

                User dataUser = _userRepository.GetById(param.UserId);

                if (dataUser == null)
                {
                    result.StatusCode = "10";
                    result.Message = "user not found";
                    return result;
                }

                if (dataUser.Isdeleted)
                {
                    result.StatusCode = "10";
                    result.Message = "user not found";
                    return result;
                }

                UserPost dataPost = new UserPost();
                dataPost.UseId = dataUser.Id;
                //dataPost.Title = param.Title;
                dataPost.Description = param.Description;

                if (param.Files != null && param.Files.Count > 0)
                {
                    string[] imageTypes = new string[] { };
                    string[] videoTypes = new string[] { };
                    int fileCount = 0;
                    dataPost.Files = param.Files.Select(f => {

                        UserPostFile file = new UserPostFile();

                        file.Extension = f.FileName.Split(".").Last();
                        file.FileName = $"{dataUser.Id}_{Guid.NewGuid().ToString()}_{DateTime.Now.Ticks.ToString()}_{fileCount}.{file.Extension}";
                        file.IsVideo = f.ContentType.ToLower().Contains("video");
                        file.IsImage = f.ContentType.ToLower().Contains("image");
                        file.Extension = f.FileName.Split(".").Last();
                        file.Size = f.Length;

                        string filePath = Path.Combine(AppSettings.UserPostFilesDir, file.FileName);

                        using (Stream fileStream = file.IsImage ? f.OpenReadStream() :  new FileStream(filePath, FileMode.Create))
                        {
                            if (file.IsImage)
                            {
                                ExtendUtil.saveAndResizeImage(fileStream, 1000, 800, filePath);
                            }
                            else
                            {
                                f.CopyTo(fileStream);
                            }
                        }

                        fileCount++;
                        return file;
                    }).ToList();
                }

                var produceUserPostRes = await KafkaUtil.ProducerKafka(
                    AppSettings.KafkaSettings.BootstrapServers,    
                    AppSettings.KafkaSettings.AddUserPostTopic,
                    JsonConvert.SerializeObject(dataPost)
                );

                result.Value = dataPost;

            }
            catch (Exception ex)
            {
                string errorMsg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                result.Message = $"Internal System Error : {errorMsg}";
                result.StatusCode = "500";
            }
            return result;
        }
        public async Task<DataPageResultModel<List<UserPost>>> GetAllPagerAsync(int from, int size)
        {
            DataPageResultModel<List<UserPost>> result = new DataPageResultModel<List<UserPost>>();
            result.StatusCode = "00";
            try
            {

                result.Data = _userPostEsRepository.GetAllPager(from, size, out long totalData);
                result.TotalData = totalData;
                result.TotalPage = totalData <= 0 ? 0 : (totalData / size);
                result.CurrentPage = (from <= 0 ? 0 : (from / size) + 1) + 1;

            }
            catch (Exception ex)
            {
                string errorMsg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                result.Message = $"Internal System Error : {errorMsg}";
                result.StatusCode = "500";
            }
            return result;
        }

        public async Task<DataPageResultModel<List<UserPost>>> GetByUserIDPagerAsync(string userId, int from, int size)
        {
            DataPageResultModel<List<UserPost>> result = new DataPageResultModel<List<UserPost>>();
            result.StatusCode = "00";
            try
            {

                result.Data = _userPostEsRepository.GetByUserIDPager(userId, from, size, out long totalData);
                result.TotalData = totalData;
                result.TotalPage = totalData <= 0 ? 0 : (totalData / size);
                result.CurrentPage = (from <= 0 ? 0 : (from / size) + 1) + 1;

            }
            catch (Exception ex)
            {
                string errorMsg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                result.Message = $"Internal System Error : {errorMsg}";
                result.StatusCode = "500";
            }
            return result;
        }

        public async Task<ServiceResultModel<UserPost>> PushNotifAddPost(ServiceResultModel<UserPost> param)
        {
            ServiceResultModel<UserPost> result = new ServiceResultModel<UserPost>();

            await this._userPostHub.Clients.All.SendAsync("PushNotifAddPost", param);

            return param;
        }

        public async Task HitPushNotifAddPost(ServiceResultModel<UserPost> param)
        {
            
        }
    }
}

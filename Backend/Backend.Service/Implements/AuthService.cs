﻿using Backend.Object.Models;
using Backend.Object.Objects;
using Backend.PsgSQLRepository.Entities;
using Backend.PsgSQLRepository.Repositories.Interfaces;
using Backend.Service.Interfaces;
using Backend.Utility.ExtendClass;
using Backend.Utility.Utilities;
using Microsoft.AspNetCore.Http;
using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Service.Implements
{
    public class AuthService : IAuthService
    {
        //repositories
        private IUserRepository _userRepository;
        private IPoolUserAccessTokenRepository _poolUserAccessTokenRepository;

        //addition class
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthService(
            IUserRepository userRepository
            , IPoolUserAccessTokenRepository poolUserAccessTokenRepository
            , IHttpContextAccessor httpContextAccessor
        )
        {
            _userRepository = userRepository;
            _poolUserAccessTokenRepository = poolUserAccessTokenRepository;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<ServiceResultModel<LoginResultObject>> LoginAsync(LoginParamObject param)
        {
            ServiceResultModel<LoginResultObject> result = new ServiceResultModel<LoginResultObject>();
            result.StatusCode = "00";
            try
            {
                //check userBy Username / email
                User user = _userRepository.GetByUserNameOrEmail(param.Email);

                if (user == null)
                {
                    result.StatusCode = "10";
                    result.Message = "user not found, username or email is not valid";
                    return result;
                }

                if (string.IsNullOrEmpty(param.Password))
                {
                    result.StatusCode = "11";
                    result.Message = "password cannot be empty";
                    return result;
                }

                //check user password hash
                if(user.Password != param.Password.HashSha256())
                {
                    result.StatusCode = "11";
                    result.Message = "password wrong";
                    return result;
                }

                string accessToken = $"{Guid.NewGuid().ToString().Replace("-", "")}#{user.Id}#{user.Email}#{user.Username}#{Guid.NewGuid().ToString().Replace("-", "")}".HashSha256();
                DateTime tokenExpiryDate = DateTime.Now.AddDays(10);

                string accessKey = JwtUtil.CreateToken(
                    AppSettings.JwtSettings.SignatureKey,
                    AppSettings.JwtSettings.Issuer,
                    AppSettings.JwtSettings.Audience,
                    tokenExpiryDate,
                    new Claim("UserId", user.Id),
                    new Claim("Email", user.Email),
                    new Claim("Username", user.Username),
                    new Claim("Token", accessToken)
                );

                //insert useraccess token
                var dataInsertToken = new Pooluseraccesstoken()
                {
                    Userid = user.Id,
                    Token = accessToken.HashSha256(),
                    Expireddate = DateTime.Now.AddMinutes(60)
                };
                var insertAccessTokenRes = _poolUserAccessTokenRepository.Insert(dataInsertToken);

                if (!insertAccessTokenRes.IsSuccess)
                {
                    result.StatusCode = "12";
                    result.Message = "insert token failed : "+insertAccessTokenRes.Message;
                    return result;
                }

                _httpContextAccessor.HttpContext.Response.Cookies.Append(
                    "SosmedAccesToken"
                    , accessKey
                    , new CookieOptions()
                    {
                        Expires = DateTime.Now.AddDays(10),
                        Path = "/",
                    }
                );


                result.Value = new LoginResultObject()
                {
                    UserId = user.Id,
                    Username = user.Username,
                    Email = user.Email,
                    AccessKey = accessToken
                };
            }
            catch(Exception ex)
            {
                result.StatusCode = "500";
                string errorMsg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                result.Message = $"Internal Server EROR : {errorMsg}";
            }

            return result;
        }

        public async Task<bool> AuthenticationAsync(string token)
        {
            bool result = false;
            try
            {



            }catch(Exception ex)
            {
                result = false;
            }
            return result;
        }

        public async Task<ServiceResultModel<DataUserObject>> GetUserByAccessTokenAsync(string token)
        {
            ServiceResultModel<DataUserObject> result = new ServiceResultModel<DataUserObject>();
            try
            {
                var process = this.GetUserByAccessTokenProcess(token, out var claimToken);

                if (process.Value == null)
                {
                    return process;
                }

                if (process.Value.IsExpired)
                {
                    result.Message = "token has been expired";
                    result.StatusCode = "14";
                    return result;
                }

                result = process;

                var dbg = "";
            }
            catch (Exception ex)
            {
                result.Message = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                result.StatusCode = "500";
            }
            return result;
        }

        public async Task<ServiceResultModel<DataUserObject>> RefreshAccessTokenAsync(string token)
        {
            ServiceResultModel<DataUserObject> result = new ServiceResultModel<DataUserObject>();
            try
            {
                var process = this.GetUserByAccessTokenProcess(token, out var claimToken);

                if (process.Value == null || claimToken == null || process.StatusCode != "00")
                {
                    return process;
                }

                
                var userAccessToken = _poolUserAccessTokenRepository.GetByUserId(process.Value.UserId).FirstOrDefault(x => x.Token.Equals(claimToken.HashSha256()));
                userAccessToken.Expireddate = DateTime.Now.AddMinutes(45);
                var updateRes = _poolUserAccessTokenRepository.Update(userAccessToken);

                if (!updateRes.IsSuccess)
                {
                    result.Message = $"refresh token failed : {updateRes.Message}";
                    result.StatusCode = "10";
                    return result;
                }

                result = process;

                var dbg = "";
            }
            catch (Exception ex)
            {
                result.Message = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                result.StatusCode = "500";
            }
            return result;
        }

        public ServiceResultModel<DataUserObject> GetUserByAccessTokenProcess(string token, out string claimToken)
        {
            ServiceResultModel<DataUserObject> result = new ServiceResultModel<DataUserObject>();
            claimToken = null;
            try
            {
                if(string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token))
                {
                    result.Message = "token cannot be empty";
                    result.StatusCode = "10";
                    return result;
                }

                var claims = JwtUtil.GetClaims(token);
                var dataTokenClaims = new DataTokenClaims();
                typeof(DataTokenClaims).GetProperties().ToList().ForEach(p => {
                    
                    string val = claims.First(c => c.Type == p.Name).Value.ToString();
                    dataTokenClaims.GetType().GetProperty(p.Name, BindingFlags.Public | BindingFlags.Instance).SetValue(dataTokenClaims, val, null);
                });

                if(string.IsNullOrEmpty(dataTokenClaims.Token) || string.IsNullOrWhiteSpace(dataTokenClaims.Token))
                {
                    result.Message = "token is not valid";
                    result.StatusCode = "11";
                    return result;
                }

                var poolToken = this._poolUserAccessTokenRepository.GetByUserId(dataTokenClaims.UserId).FirstOrDefault(x => x.Token == dataTokenClaims.Token.HashSha256());
                if (poolToken == null)
                {
                    result.Message = "token is not valid";
                    result.StatusCode = "12";
                    return result;
                }

                User dataUser = this._userRepository.GetById(poolToken.Userid);
                if(dataUser == null)
                {
                    result.Message = "data user is not found";
                    result.StatusCode = "13";
                    return result;
                }

                result.Value = new DataUserObject(dataTokenClaims);
                result.Value.IsExpired = poolToken.Expireddate <= DateTime.Now;

                result.StatusCode = "00";
                claimToken = dataTokenClaims.Token;
                var dbg = "";
            }
            catch (Exception ex)
            {
                result.Message = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                result.StatusCode = "500";
            }
            return result;
        }
    }
}

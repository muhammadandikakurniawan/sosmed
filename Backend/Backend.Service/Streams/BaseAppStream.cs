﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Service.Streams
{
    public class BaseAppStream<T> : IBaseAppStream<T>
    {

        private Subject<T> _subject;
        private IDictionary _subcribers;

        public BaseAppStream()
        {
            this._subject = new Subject<T>();
            this._subcribers = new Dictionary<string, Action<T>>();
        }

        public async Task PublishAsync(T param)
        {
            this._subject.OnNext(param);
        }


        public async Task SubscribeAsync(string subcriberName, Action<T> action)
        {
            if (!this._subcribers.Contains(subcriberName))
            {
                this._subcribers.Add(subcriberName, this._subject.Subscribe<T>(action));
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Service.Streams
{
    public interface IBaseAppStream<T> :  IDisposable
    {

        public Task PublishAsync(T param);
        public Task SubscribeAsync(string subcriberName, Action<T> action);

    }
}

﻿using Backend.ElasticSearchRepository.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Service.Streams.Interfaces
{
    public interface IUserPostStream : IDisposable
    {
        public Task PublishAddUserPostAsync(string param);
        public Task SubscribeAddUserPostAsync(string subcriberName);
    }
}

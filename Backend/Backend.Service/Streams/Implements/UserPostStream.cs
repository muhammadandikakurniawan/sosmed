﻿using Backend.ElasticSearchRepository.Entities;
using Backend.ElasticSearchRepository.Repositories.Interfaces;
using Backend.Object.Models;
using Backend.PsgSQLRepository.Repositories.Interfaces;
using Backend.Service.AppHubs;
using Backend.Service.Interfaces;
using Backend.Service.Streams.Interfaces;
using Backend.Utility.Utilities;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Service.Streams.Implements
{
    public class UserPostStream : IUserPostStream
    {
        private Subject<string> _addPostSubject;
        private IDictionary _addPostsubcribers;
        private IUserRepository _userRepository;
        private IUserPostEsRepository _userPostEsRepository;
        private IHttpContextAccessor _httpContextAccessor;

        public UserPostStream(
            IUserRepository userRepository
            , IUserPostEsRepository userPostEsRepository
            , IHttpContextAccessor httpContextAccessor
        )
        {
            _userRepository = userRepository;
            _userPostEsRepository = userPostEsRepository;

            _httpContextAccessor = httpContextAccessor;

            _addPostSubject = new Subject<string>();
            _addPostsubcribers = new Dictionary<string, string>();
        }

        public async Task PublishAddUserPostAsync(string param)
        {
            _addPostSubject.OnNext(param);
        }

        public async Task SubscribeAddUserPostAsync(string subcriberName)
        {
            if (!this._addPostsubcribers.Contains(subcriberName))
            {
                this._addPostsubcribers.Add(subcriberName, this._addPostSubject.Subscribe<string>((msg) => { SubcribeAddPost(msg); }));
            }
        }

        public async Task<ServiceResultModel<UserPost>> SubcribeAddPost(string message)
        {
            ServiceResultModel<UserPost> result = new ServiceResultModel<UserPost>();
            result.StatusCode = "00";
            try
            {

                UserPost paramAddPost = JsonConvert.DeserializeObject<UserPost>(message);

                var insertPostRes = _userPostEsRepository.Insert(paramAddPost);
                if (!insertPostRes.IsSuccess)
                {
                    result.StatusCode = "11";
                    result.Message = $"insert data failed : {insertPostRes.Message}";
                    var produceUserPostRes = await KafkaUtil.ProducerKafka(
                        AppSettings.KafkaSettings.BootstrapServers,
                        AppSettings.KafkaSettings.AddUserPostTopic,
                        JsonConvert.SerializeObject(paramAddPost)
                    );
                    return result;
                }

                result.Value = paramAddPost;

                var pushNotifRes = await HttpClientUtil.PostWithBody<ServiceResultModel<object>>("https://localhost:44301/api/UserPost/PushNotifAddPost", result);

            }
            catch (Exception ex)
            {
                string errorMsg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                result.Message = $"Internal System Error : {errorMsg}";
                result.StatusCode = "500";
            }
            return result;
        }

        public void Dispose()
        {

            if (this._addPostSubject != null)
            {
                this._addPostSubject.Dispose();
            }

            foreach (var s in this._addPostsubcribers)
            {

            }

        }
    }
}

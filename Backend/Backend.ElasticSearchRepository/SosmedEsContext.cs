﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;
using Backend.Utility.Utilities;
using Elasticsearch.Net;
using Nest.JsonNetSerializer;

namespace Backend.ElasticSearchRepository
{
    public class SosmedEsContext : IDisposable
    {

        private List<Uri> _nodes = new List<Uri>();
        private ElasticClient _client;
        public ElasticClient Client { get { return this._client; } }

        public SosmedEsContext()
        {
            var ElasticSerachSettings = AppSettings.ElasticSerachSettings;

            _nodes.Add(new Uri(ElasticSerachSettings.ElasticSerachURL));

            var connectionPool = new StaticConnectionPool(this._nodes);
            var connectionConfig = new ConnectionSettings(connectionPool);
            connectionConfig.PrettyJson();
            connectionConfig.DefaultFieldNameInferrer(p => p);
            this._client = new ElasticClient(connectionConfig);
            //var allindex = this._client.Indices.Get(new GetIndexRequest(Indices.All)).Indices.Keys;
        }

        ~SosmedEsContext()
        {
            Console.WriteLine("An Instance of class destroyed");
        }
        public void Dispose()
        {
            this._client = null;
            this._nodes = null;
        }
    }

}

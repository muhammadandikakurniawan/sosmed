﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Backend.ElasticSearchRepository.Entities
{
    public class UserPost
    {

        [JsonProperty("Id"), NotNull()]
        public string Id { get; set; }

        [JsonProperty("UseId"), NotNull()]
        public string UseId { get; set; }

        [JsonProperty("Title"), NotNull()]
        public string Title { get; set; }

        [JsonProperty("Description"), NotNull()]
        public string Description { get; set; }

        [JsonProperty("Files"), NotNull()]
        public List<UserPostFile> Files { get; set; }

        [JsonProperty("CreatedDate"), NotNull()]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("IsDeleted"), NotNull()]
        public bool IsDeleted { get; set; }

        public UserPost()
        {

        }
    }

    public class UserPostFile
    {

        [JsonProperty("FileName"), NotNull()]
        public string FileName { get; set; }

        [JsonProperty("Extension"), NotNull()]
        public string Extension { get; set; }

        [JsonProperty("Size"), NotNull()]
        public long Size { get; set; }

        [JsonProperty("IsImage"), NotNull()]
        public bool IsImage { get; set; }

        [JsonProperty("IsVideo"), NotNull()]
        public bool IsVideo { get; set; }

    }
}

﻿using Backend.ElasticSearchRepository.Entities;
using Backend.ElasticSearchRepository.Repositories;
using Backend.ElasticSearchRepository.Repositories.Implements;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Backend.ElasticSearchRepository
{
    public class Program
    {
        static async Task Main(string[] args)
        {

            var repo = new UserPostEsRepository();

            UserPost data = new UserPost();
            data.UseId = "46e591c5d2e14284a14cb9250952858f";
            data.Title = Faker.Lorem.Words(1).FirstOrDefault();
            data.Description = Faker.Lorem.Paragraph();
            data.Files = new List<UserPostFile>();
            data.Files.Add(
                new UserPostFile()
                {
                    FileName = Faker.Lorem.Words(1).FirstOrDefault(),
                    Extension = Faker.Lorem.Words(1).FirstOrDefault(),
                    Size = 12000
                }
            );

            var insertRes = repo.Insert(data);

            //var getData = await repo.GetByIDAsync("e73e236b0b4941d481cc646eed148fee");
            //getData.Title = "test edit title2";
            //var updateRes = repo.Update(getData);

            Console.WriteLine("Hello World!");
        }
    }
}

﻿using Backend.ElasticSearchRepository.Entities;
using Backend.Object.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.ElasticSearchRepository.Repositories
{
    public class BaseEsRepository<T>
    {
        public RepoResultModel<T> Insert(T param, string indexName)
        {
            var result = new RepoResultModel<T>();
            try
            {
                using (var ctx = new SosmedEsContext())
                {
                    var client = ctx.Client;

                    var indexResponse = client.Index<object>(param,conf => conf.Index(indexName));
                    result.Result = param;
                }
            }
            catch(Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.InnerException.Message;
            }

            return result;
        }

        public RepoResultModel<T> Update(T param, string indexName)
        {
            var result = new RepoResultModel<T>();
            try
            {
                using (var ctx = new SosmedEsContext())
                {
                    var client = ctx.Client;

                    var indexResponse = client.Update<object>(param, conf => conf
                        .Index(indexName)
                        .Doc(param)
                    );
                    result.Result = param;
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.InnerException.Message;
            }

            return result;
        }

        public RepoResultModel<T> Delete(T param, string indexName)
        {
            var result = new RepoResultModel<T>();
            try
            {
                using (var ctx = new SosmedEsContext())
                {
                    var client = ctx.Client;

                    var indexResponse = client.Delete<object>(param, conf => conf.Index(indexName));
                    result.Result = param;
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.InnerException.Message;
            }

            return result;
        }
    }
}

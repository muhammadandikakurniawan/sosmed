﻿using Backend.ElasticSearchRepository.Entities;
using Backend.Object.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.ElasticSearchRepository.Repositories.Interfaces
{
    public interface IUserPostEsRepository
    {
        public RepoResultModel<UserPost> Insert(UserPost param);
        public List<UserPost> GetAllPager(int from, int size, out long total);
        public List<UserPost> GetByUserIDPager(string id, int from, int size, out long total);
    }
}

﻿using Backend.ElasticSearchRepository.Entities;
using Backend.ElasticSearchRepository.Repositories.Interfaces;
using Backend.Object.Models;
using Nest;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Backend.ElasticSearchRepository.Repositories.Implements
{
    public class UserPostEsRepository : BaseEsRepository<UserPost>, IUserPostEsRepository
    {
        private string _indexName { get { return "sosmed_user_post"; } }
        public RepoResultModel<UserPost> Insert(UserPost param)
        {
            param.Id = Guid.NewGuid().ToString().Replace("-", "");
            param.CreatedDate = DateTime.Now;
            param.IsDeleted = false;

            return base.Insert(param, _indexName);
        }

        public RepoResultModel<UserPost> Update(UserPost param)
        {

            return base.Update(param, _indexName);
        }

        public RepoResultModel<UserPost> Delete (UserPost param)
        {

            return base.Delete(param, _indexName);
        }

        public RepoResultModel<UserPost> SoftDelete(UserPost param)
        {
            param.IsDeleted = true;
            return base.Update(param, _indexName);
        }

        public async Task<UserPost> GetByIDAsync(string id)
        {
            UserPost result = null;
            try
            {

                using (var ctx = new SosmedEsContext())
                {
                    List<QueryContainer> queryContainers = new List<QueryContainer>();

                    queryContainers.Add(
                        new MatchQuery() { Field = "Id.keyword", Query = id }
                    );

                    var client = ctx.Client;
                    var searchRes = await client.SearchAsync<UserPost>(s => s
                        .Index(_indexName)
                        .Query(q => q
                            .Bool(qb => qb
                                .Must(queryContainers.ToArray())
                            )
                        )
                    );

                    result = searchRes.Hits.FirstOrDefault() != null ? searchRes.Hits.FirstOrDefault().Source : result;
                }

            }catch(Exception ex)
            {
                var msg = ex.InnerException.Message;
            }
            return result;
        }

        public List<UserPost> GetByUserIDPager(string id, int from, int size, out long total)
        {
            List<UserPost> result = null;
            total = 0;

            using (var ctx = new SosmedEsContext())
            {
                List<QueryContainer> queryContainers = new List<QueryContainer>();

                queryContainers.Add(
                    new MatchQuery() { Field = "UseId.keyword", Query = id }
                );

                var client = ctx.Client;
                var searchRes = client.Search<UserPost>(s => s
                    .Index(_indexName)
                    .Query(q => q
                        .Bool(qb => qb
                            .Must(queryContainers.ToArray())
                        )
                    )
                    .From(from)
                    .Size(size)
                    .Sort(s => s.Descending(f => f.CreatedDate))
                );

                result = searchRes.Hits.FirstOrDefault() != null ? searchRes.Hits.Select(h => h.Source).ToList() : result;

                total = client.Count<UserPost>(c => c
                    .Index(_indexName)
                    .Query(q => q
                        .Bool(qb => qb
                            .Must(queryContainers.ToArray())
                        )
                    )
                ).Count;
            }

            return result;
        }

        public List<UserPost> GetAllPager(int from, int size, out long total)
        {
            List<UserPost> result = new List<UserPost>();

            total = 0;

            using(var ctx = new SosmedEsContext())
            {
                var client = ctx.Client;

                var searchRes = client.Search<UserPost>(s => s
                    .Index(_indexName)
                    .Query(q => q.MatchAll())
                    .From(from)
                    .Size(size)
                    .Sort(s => s.Descending(f => f.CreatedDate))
                );

                result = searchRes.Hits.FirstOrDefault() != null ? searchRes.Hits.Select(h => h.Source).ToList() : result;
                total = client.Count<UserPost>(c => c
                    .Index(_indexName)
                    .Query(q => q.MatchAll())
                ).Count;
            }

            return result;
        }
    }
}

﻿using Backend.ElasticSearchRepository.Repositories.Implements;
using Backend.ElasticSearchRepository.Repositories.Interfaces;
using Backend.Object.Objects;
using Backend.PsgSQLRepository.Repositories.Implements;
using Backend.PsgSQLRepository.Repositories.Interfaces;
using Backend.Service.Implements;
using Backend.Service.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using Xunit;

namespace Backend.Test
{
    public class DependencySetupFixture
    {
        public DependencySetupFixture()
        {
            var services = new ServiceCollection();
            //======================================================= dependency injection ===========================================================
            //================= Service ========================
            services.AddSingleton<IAuthService, AuthService>();
            services.AddSingleton<IUserPostService, UserPostService>();
            //================= END Service ====================

            //================= Repository =====================
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IPoolUserAccessTokenRepository, PoolUserAccessTokenRepository>();
            services.AddSingleton<IUserPostEsRepository, UserPostEsRepository>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            //================= END Repository =================
            //======================================================= END dependency injection =======================================================



            ServiceProvider = services.BuildServiceProvider();
        }

        public ServiceProvider ServiceProvider { get; private set; }
    }
}

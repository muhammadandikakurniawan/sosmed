﻿using Backend.ElasticSearchRepository.Repositories.Implements;
using Backend.ElasticSearchRepository.Repositories.Interfaces;
using Backend.MessageConsumer.Consumers.Implements;
using Backend.MessageConsumer.Consumers.Interfaces;
using Backend.PsgSQLRepository.Repositories.Implements;
using Backend.PsgSQLRepository.Repositories.Interfaces;
using Backend.Service.Implements;
using Backend.Service.Interfaces;
using Backend.Service.Streams.Implements;
using Backend.Service.Streams.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Builder.Internal;
using Backend.Service.AppHubs;
using Microsoft.AspNet.SignalR;
using Backend.PsgSQLRepository.Entities;
using System.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Backend.MessageConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            var services = new ServiceCollection();

            //===================================== consumers ===================================================
            services.AddTransient<IUserPostConsumer, UserPostConsumer>();
            //===================================== end consumers ===============================================

            //===================================== streams =====================================================
            services.AddTransient<IUserPostStream, UserPostStream>();
            //===================================== end streams =================================================

            //===================================== repositories ================================================
            services.AddDbContext<SosmedContext>(options => options.UseNpgsql("Host=localhost;Database=Sosmed;Username=postgres;Password=admin"),
                ServiceLifetime.Transient
             );
            services.AddTransient<IUserPostEsRepository, UserPostEsRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            //===================================== end repositories ============================================

            IServiceProvider serviceProvider = services.BuildServiceProvider();

            //======================================================== Consumer =================================
            serviceProvider.GetService<IUserPostConsumer>().ListenInsertUserPost();
            //======================================================== END Consumer =============================
















            //if (serviceProvider is IDisposable)
            //{
            //    ((IDisposable)serviceProvider).Dispose();
            //}

            Console.ReadKey();
        }
    }
}

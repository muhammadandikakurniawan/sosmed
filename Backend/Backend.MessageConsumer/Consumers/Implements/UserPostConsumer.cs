﻿using Backend.MessageConsumer.Consumers.Interfaces;
using Backend.Service.Streams.Interfaces;
using Backend.Utility.Utilities;
using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Backend.MessageConsumer.Consumers.Implements
{
    public class UserPostConsumer : IUserPostConsumer
    {
        private IUserPostStream _userPostStream;
        public UserPostConsumer(
            IUserPostStream userPostStream
        )
        {
            _userPostStream = userPostStream;
        }
        public async Task ListenInsertUserPost()
        {
            var config = new ConsumerConfig
            {
                GroupId = "sosmed_add_user_post_group",
                BootstrapServers = AppSettings.KafkaSettings.BootstrapServers,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                EnableAutoCommit = true
            };

            using (var consumer = new ConsumerBuilder<Ignore, string>(config).Build())
            {
                consumer.Subscribe(AppSettings.KafkaSettings.AddUserPostTopic);

                var cts = new CancellationTokenSource();
                Console.CancelKeyPress += (_, e) => {
                    e.Cancel = true;
                    cts.Cancel();
                };

                try
                {

                    this._userPostStream.SubscribeAddUserPostAsync("addPostSubcriber");

                    while (true)
                    {
                        var msg = await Task.Run(() => consumer.Consume(cts.Token));
                        await this._userPostStream.PublishAddUserPostAsync(msg.Value);
                        Console.WriteLine("========================= ListenInsertUserPost =========================");
                        Console.WriteLine(msg.Value);
                    }


                }
                catch (Exception ex)
                {
                    Console.WriteLine("========================= ListenInsertUserPost ERROR =========================");
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}

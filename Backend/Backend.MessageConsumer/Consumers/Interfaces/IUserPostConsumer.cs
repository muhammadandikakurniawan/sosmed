﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Backend.MessageConsumer.Consumers.Interfaces
{
    public interface IUserPostConsumer
    {

        public Task ListenInsertUserPost();

    }
}

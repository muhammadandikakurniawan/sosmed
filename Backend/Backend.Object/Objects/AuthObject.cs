﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.Object.Objects
{
    public class LoginParamObject
    {
        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }
    }

    public class LoginResultObject
    {
        [JsonProperty("AccessKey")]
        public string AccessKey { get; set; }

        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("UserId")]
        public string UserId { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }
    }


    public class DataTokenClaims
    {
        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("Id")]
        public string UserId { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Token")]
        public string Token { get; set; }
    }

    public class DataUserObject
    {

        public DataUserObject(DataTokenClaims param)
        {

            this.UserId = param.UserId;
            this.Username = param.Username;
            this.Email = param.Email;

        }
        public DataUserObject()
        {

        }

        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("Id")]
        public string UserId { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("IsExpired")]
        public bool IsExpired { get; set; }
    }
}

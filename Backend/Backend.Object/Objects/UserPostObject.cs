﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Backend.Object.Objects
{
    public class AddPostParamObject
    {

        [JsonProperty(PropertyName = "UserId"), NotNull()]
        public string UserId { get; set; }

        //[JsonProperty(PropertyName = "Title"), NotNull()]
        //public string Title { get; set; }

        [JsonProperty(PropertyName = "Description"), NotNull()]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "Files"), NotNull()]
        public List<IFormFile> Files { get; set; }
    }
}

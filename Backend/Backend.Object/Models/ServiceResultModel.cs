﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.Object.Models
{
    public class ServiceResultModel<T>
    {
        [JsonProperty("StatusCode")]
        public string StatusCode { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("Value")]
        public T Value { get; set; }
    }
}

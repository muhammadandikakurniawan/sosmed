﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.Object.Models
{
    public class DataPagerParameterModel
    {
        [JsonProperty("From")]
        public int From { get; set; }

        [JsonProperty("Size")]
        public int Size { get; set; }
    }

    public class DataPagerParameterModel<T>
    {
        [JsonProperty("From")]
        public int From { get; set; }

        [JsonProperty("Size")]
        public int Size { get; set; }

        [JsonProperty("AdditionParamValue")]
        public T AdditionParamValue { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.Object.Models
{
    public class DataPageResultModel<T>
    {
        [JsonProperty("Page")]
        public long Page = 0;

        [JsonProperty("TotalData")]
        public long TotalData { get; set; }

        [JsonProperty("TotalPage")]
        public long TotalPage { get; set; }

        [JsonProperty("CurrentPage")]
        public long CurrentPage { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("StatusCode")]
        public string StatusCode { get; set; }

        [JsonProperty("Data")]
        public T Data { get; set; }
    }
}

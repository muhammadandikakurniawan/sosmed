﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.Object.Models
{
    public class HttpClientResultModel<T>
    {

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("StatusCode")]
        public string StatusCode { get; set; }

        [JsonProperty("IsSuccess")]
        public bool IsSuccess { get; set; }

        [JsonProperty("Result")]
        public T Result { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.Object.Models
{
    public class RepoResultModel<T>
    {
        [JsonProperty("IsSuccess")]
        public bool IsSuccess = true;

        [JsonProperty( "Message")]
        public string Message { get; set; }

        [JsonProperty("Result")]
        public T Result { get; set; }
    }
}

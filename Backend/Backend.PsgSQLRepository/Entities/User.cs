﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Backend.PsgSQLRepository.Entities
{
    public partial class User
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime Createddate { get; set; }
        public bool Isdeleted { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace Backend.PsgSQLRepository.Entities
{
    public partial class Pooluseraccesstoken
    {
        public string Id { get; set; }
        public string Userid { get; set; }
        public string Token { get; set; }
        public DateTime Createddate { get; set; }
        public DateTime Expireddate { get; set; }
        public bool Isdeleted { get; set; }
    }
}

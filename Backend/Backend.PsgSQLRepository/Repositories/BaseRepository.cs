﻿using Backend.Object.Models;
using Backend.PsgSQLRepository.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Backend.PsgSQLRepository.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T>
    {
        protected SosmedContext dbContext;
        public SosmedContext DbContext { get {return this.dbContext;} }

        public BaseRepository(SosmedContext dbContextParam)
        {
            this.dbContext = dbContextParam;
        }

        public IQueryable<T> FindAll()
        {
            MethodInfo method = typeof(DbContext).GetMethods()
            .Where(p => p.Name == "Set" && p.ContainsGenericParameters).FirstOrDefault();

            // Build a method with the specific type argument you're interested in
            method = method.MakeGenericMethod(typeof(T));

            return method.Invoke(dbContext, null) as IQueryable<T>;
        }

        public RepoResultModel<T> Insert(T param)
        {
            RepoResultModel<T> result = new RepoResultModel<T>();
            try
            {
                var addRes = dbContext.Add(param);
                var saveChangeRes = dbContext.SaveChanges();

                if (saveChangeRes <= 0)
                {
                    throw new Exception("insert failed");
                }
                result.Result = param;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return result;
        }

        public RepoResultModel<T> Update(T param)
        {
            RepoResultModel<T> result = new RepoResultModel<T>();
            try
            {
                var addRes = dbContext.Update(param);
                var saveChangeRes = dbContext.SaveChanges();

                if (saveChangeRes <= 0)
                {
                    throw new Exception("update failed");
                }
                result.Result = param;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }
            return result;
        }

        public RepoResultModel<T> Delete(T param)
        {
            RepoResultModel<T> result = new RepoResultModel<T>();
            try
            {
                var addRes = dbContext.Remove(param);
                var saveChangeRes = dbContext.SaveChanges();

                if (saveChangeRes <= 0)
                {
                    throw new Exception("delete failed");
                }
                result.Result = param;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }
            return result;
        }

        public async Task<RepoResultModel<T>> InsertAsync(T param)
        {
            RepoResultModel<T> result = new RepoResultModel<T>();
            try
            {
                await dbContext.AddAsync(param);
                await dbContext.SaveChangesAsync();
                result.Result = param;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }
            return result;
        }

    }
}

﻿using Backend.PsgSQLRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Backend.PsgSQLRepository.Repositories.Interfaces
{
    public interface IPoolUserAccessTokenRepository : IBaseRepository<Pooluseraccesstoken>
    {
        public IQueryable<Pooluseraccesstoken> GetByUserId(string userId);
        public Pooluseraccesstoken GetLatestByUserId(string userId);
        public IQueryable<Pooluseraccesstoken> GetAll();
    }
}

﻿using Backend.PsgSQLRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Backend.PsgSQLRepository.Repositories.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {
        public User GetByUserNameOrEmail(string username, string email);
        public User GetByUserNameOrEmail(string param);
        public User GetByAccessKey(string param);
        public IQueryable<User> GetAll();
        public User GetById(string userId);
    }
}

﻿using Backend.Object.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.PsgSQLRepository.Repositories
{
    public interface IBaseRepository<T>
    {
        public RepoResultModel<T> Insert(T param);
        public RepoResultModel<T> Update(T param);
        public RepoResultModel<T> Delete(T param);
    }
}

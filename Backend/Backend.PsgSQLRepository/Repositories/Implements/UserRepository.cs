﻿using Backend.Object.Models;
using Backend.PsgSQLRepository.Entities;
using Backend.PsgSQLRepository.Repositories.Interfaces;
using Backend.Utility.ExtendClass;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Backend.PsgSQLRepository.Repositories.Implements
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {

        public UserRepository(SosmedContext dbContextParam) : base(dbContextParam)
        {

        }

        public RepoResultModel<User> Insert(User param)
        {
            param.Id = (Guid.NewGuid()).ToString().Replace("-",string.Empty);
            param.Createddate = DateTime.Now;
            param.Isdeleted = false;

            return base.Insert(param);
        }

        public RepoResultModel<User> Update(User param)
        {
            return base.Update(param);
        }

        public RepoResultModel<User> Delete(User param)
        {
            return base.Delete(param);
        }

        public User GetByUserNameOrEmail(string username, string email)
        {
            return base.FindAll().FirstOrDefault(u => u.Username.Equals(username) || u.Email.Equals(email));
        }

        public User GetByUserNameOrEmail(string param)
        {
            return base.FindAll().FirstOrDefault(u => u.Username.Equals(param) || u.Email.Equals(param));
        }

        public User GetByAccessKey(string param)
        {
            return base.FindAll().FirstOrDefault(u => (u.Id + "#" + u.Email + "#" + u.Username).HashSha256().Equals(param) );
        }

        public IQueryable<User> GetAll()
        {
            return base.FindAll();
        }

        public User GetById(string userId)
        {
            return base.FindAll().FirstOrDefault(u => u.Id.Equals(userId));
        }
    }
}

﻿using Backend.Object.Models;
using Backend.PsgSQLRepository.Entities;
using Backend.PsgSQLRepository.Repositories.Interfaces;
using Backend.Utility.ExtendClass;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Backend.PsgSQLRepository.Repositories.Implements
{
    public class PoolUserAccessTokenRepository : BaseRepository<Pooluseraccesstoken>, IPoolUserAccessTokenRepository
    {

        public PoolUserAccessTokenRepository(SosmedContext dbContextParam):base(dbContextParam)
        {

        }
        public RepoResultModel<Pooluseraccesstoken> Insert(Pooluseraccesstoken param)
        {
            param.Id = (Guid.NewGuid()).ToString().Replace("-",string.Empty);
            param.Createddate = DateTime.Now;
            param.Isdeleted = false;

            return base.Insert(param);
        }

        public RepoResultModel<Pooluseraccesstoken> Update(Pooluseraccesstoken param)
        {
            return base.Update(param);
        }

        public RepoResultModel<Pooluseraccesstoken> Delete(Pooluseraccesstoken param)
        {
            return base.Delete(param);
        }

        public IQueryable<Pooluseraccesstoken> GetByUserId(string userId)
        {
            return base.FindAll().Where(x => x.Userid == userId);
        }

        public Pooluseraccesstoken GetLatestByUserId(string userId)
        {
            return base.FindAll().Where(x => x.Userid == userId).OrderByDescending(x => x.Createddate).FirstOrDefault();
        }

        public IQueryable<Pooluseraccesstoken> GetAll()
        {
            return base.FindAll();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.ElasticSearchRepository.Entities;
using Backend.Object.Models;
using Backend.Service.Interfaces;
using Backend.Utility.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.SignalRServiceAPI.Controllers
{
    [Route("api/UserPost")]
    [ApiController]
    public class UserPostController : ControllerBase
    {
        private IUserPostService _userPostService;
        private readonly IHttpContextAccessor _httpContext;
        public UserPostController(
            IUserPostService userPostService,
            IHttpContextAccessor httpContext
        )
        {
            _userPostService = userPostService;
            _httpContext = httpContext;
        }

        [HttpPost]
        [Route("PushNotifAddPost")]
        public async Task<ServiceResultModel<UserPost>> PushNotifAddPost([FromBody] ServiceResultModel<UserPost> param)
        {
            return await this._userPostService.PushNotifAddPost(param);
        }


        [HttpPost]
        [Route("HitPushNotifAddPost")]
        public async Task<Dictionary<string,string>> HitPushNotifAddPost([FromBody] ServiceResultModel<UserPost> param)
        {
            await HttpClientUtil.PostWithBody<ServiceResultModel<object>>("https://localhost:44301/api/UserPost/PushNotifAddPost", param);

            return new Dictionary<string, string>()
            {
                {"Status","on progress" }
            };
        }
    }
}

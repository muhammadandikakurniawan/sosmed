﻿
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Backend.SignalRServiceAPI.AppHubs
{
    public class UserPostHub : Hub
    {
        public async Task Add(string param)
        {
            await Clients.All.SendAsync("Add", param);
        }
    }
}

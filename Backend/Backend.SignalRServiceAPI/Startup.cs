using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Backend.ElasticSearchRepository.Repositories.Implements;
using Backend.ElasticSearchRepository.Repositories.Interfaces;
using Backend.PsgSQLRepository.Entities;
using Backend.PsgSQLRepository.Repositories.Implements;
using Backend.PsgSQLRepository.Repositories.Interfaces;
using Backend.Service.Implements;
using Backend.Service.Interfaces;
using Backend.Utility.Utilities;
using Microsoft.EntityFrameworkCore;
using Backend.Service.AppHubs;

namespace Backend.SignalRServiceAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //======================================================= dependency injection ===========================================================
            //================= Service ========================
            services.AddTransient<IAuthService, AuthService>();
            services.AddTransient<IUserPostService, UserPostService>();
            //================= END Service ====================

            //================= Repository =====================
            services.AddDbContext<SosmedContext>(options => options.UseNpgsql(Configuration.GetValue<string>("ConnectionString:SosmedDb")),
                ServiceLifetime.Transient
             );
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IPoolUserAccessTokenRepository, PoolUserAccessTokenRepository>();
            services.AddTransient<IUserPostEsRepository, UserPostEsRepository>();

            //================= END Repository =================
            //======================================================= END dependency injection =======================================================



            services.AddCors(config => {
                config.AddDefaultPolicy(policy => {
                    policy.SetIsOriginAllowed(_ => true).AllowCredentials().AllowAnyHeader().AllowAnyMethod();
                });
            });


            services.AddControllersWithViews()
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddControllers()
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddHttpContextAccessor();
            services.AddControllersWithViews();
            services.AddSignalR();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSignalR(routes => {
                    routes.MapHub<UserPostHub>("/UserPostHub");
                }
            );
            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

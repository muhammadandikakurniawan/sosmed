﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Backend.Utility.Utilities;

namespace Backend.Utility.ExtendClass
{
    public static class StringExtend
    {

        public static string HashSha256(this string str)
        {
            return StringUtil.Hash(str, SHA256.Create());
        }

    }
}

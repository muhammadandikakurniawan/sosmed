﻿using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Utility.Utilities
{
    public static class KafkaUtil
    {

        public static async Task<DeliveryResult<string, string>> ProducerKafka(string bootstrapServers, string topicName, string payload)
        {
            var result = new DeliveryResult<string, string>();
            var config = new ProducerConfig
            {
                BootstrapServers = bootstrapServers,
            };

            using (
                var producer = new ProducerBuilder<string, string>(config)
                .Build()
            )
            {
                var msg = new Message<string, string>
                {
                    Value = payload
                };

                var send = await producer.ProduceAsync(topicName, msg);

                result = send;

            }

            return result;
        }

    }
}

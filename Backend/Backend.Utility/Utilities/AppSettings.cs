﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace Backend.Utility.Utilities
{
    public static class AppSettings
    {
        public static T GetAppSettings<T>(string key)
        {
            T result = default(T);

            var currentDir = Directory.GetCurrentDirectory();
            var configBuilder = new ConfigurationBuilder()
                .AddJsonFile(Path.Combine(currentDir, "appsettings.json")).Build();
            result = (T)Convert.ChangeType(configBuilder[key], typeof(T));

            return result;
        }

        public static ElasticSerachSettings ElasticSerachSettings { 
            get {
                return new ElasticSerachSettings(
                    GetAppSettings<string>("ElasticSerachSettings:ElasticSerachURL")
                    ,GetAppSettings<string>("ElasticSerachSettings:ESUsertatusIndex")
                );
            }
        }

        public static KafkaSettings KafkaSettings
        {
            get
            {
                return new KafkaSettings(
                    GetAppSettings<string>("KafkaSettings:BootstrapServers"),
                    GetAppSettings<string>("KafkaSettings:AddUserPostTopic"),
                    GetAppSettings<string>("KafkaSettings:UpdateUserPostTopic"),
                    GetAppSettings<string>("KafkaSettings:DeleteUserPostTopic")
                );

            }
        }

        public static JwtSettings JwtSettings
        {

            get
            {
                return new JwtSettings(
                    GetAppSettings<string>("JwtSettings:Issuer"),
                    GetAppSettings<string>("JwtSettings:Audience"),
                    GetAppSettings<string>("JwtSettings:SignatureKey")
                );

            }
        }

        public static string ElasticSerachURL { 
            get {
                return GetAppSettings<string>("ElasticSerachURL");    
            } 
        }
        public static string ESUsertatusIndex
        {
            get
            {
                return GetAppSettings<string>("ESUsertatusIndex");
            }
        }

        public static string UserPostFilesDir
        {
            get
            {
                return GetAppSettings<string>("UserPostFilesDir");
            }
        }


    }


    public class KafkaSettings
    {
        private string _bootstrapServers { get; set; }
        public string BootstrapServers { get { return this._bootstrapServers; } }

        private string _addUserPostTopic { get; set; }
        public string AddUserPostTopic { get { return this._addUserPostTopic; } }

        private string _updateUserPostTopic { get; set; }
        public string UpdateUserPostTopic { get { return this._updateUserPostTopic; } }

        private string _deleteUserPostTopic { get; set; }
        public string DeleteUserPostTopic { get { return this._deleteUserPostTopic; } }

        public KafkaSettings(
            string bootstrapServers,
            string addUserPostTopic,
            string updateUserPostTopic,
            string deleteUserPostTopic
        )
        {
            this._bootstrapServers = bootstrapServers;
            this._addUserPostTopic = addUserPostTopic;
            this._updateUserPostTopic = updateUserPostTopic;
            this._deleteUserPostTopic = deleteUserPostTopic;
        }

    }
    public class ElasticSerachSettings
    {

        private string _elasticSerachURL {get;set;}
        private string _eSUsertatusIndex { get; set; }

        public string ElasticSerachURL { get { return this._elasticSerachURL; } }
        public string ESUsertatusIndex { get { return this._eSUsertatusIndex; } }
        public ElasticSerachSettings(
            string ElasticSerachURLParam,
            string ESUsertatusIndexParam
        )
        {
            this._elasticSerachURL = ElasticSerachURLParam;
            this._eSUsertatusIndex = ESUsertatusIndexParam;
        }
    }

    public class JwtSettings
    {
        private string _issuer { get; set; }
        private string _audience { get; set; }
        private string _signatureKey { get; set; }

        public string Issuer { get { return this._issuer; } }
        public string Audience { get { return this._audience; } }
        public string SignatureKey { get { return this._signatureKey; } }
        public JwtSettings(
            string issuer,
            string audience,
            string signatureKey
        )
        {
            this._issuer = issuer;
            this._audience  = audience;
            this._signatureKey = signatureKey;
        }
    }
}

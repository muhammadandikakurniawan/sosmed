﻿using Backend.Object.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Utility.Utilities
{
    public static class HttpClientUtil
    {

        public static async Task<HttpClientResultModel<T>> PostWithBody<T>(string url, object bodyParam)
        {

            HttpClientResultModel<T> result = new HttpClientResultModel<T>();
            result.IsSuccess = true;
            try
            {

                var client = new HttpClient();
                var body = new StringContent(JsonConvert.SerializeObject(bodyParam), Encoding.UTF8, "application/json");
                var response = await client.PostAsync(url, body);

                result.Message = await response.RequestMessage.Content.ReadAsStringAsync();
                result.StatusCode = response.StatusCode.ToString();

                if (response.IsSuccessStatusCode)
                {

                    string httpResult = response.Content.ReadAsStringAsync().Result;

                    var bodyParse = JsonConvert.DeserializeObject<T>(httpResult);

                    var dbg = "";
                }
                else
                {
                    result.IsSuccess = false;
                    return result;
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                result.Message = errorMessage;
                result.StatusCode = "500";
                result.IsSuccess = false;
            }

            return result;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Utility.Utilities
{
    public static class ExtendUtil
    {
        public static void saveAndResizeImage(Stream strm, int width, int height, string dstPath)
        {
            Image image = System.Drawing.Image.FromStream(strm, true, true);
            Image resizedImage = image.GetThumbnailImage(width, height, null, IntPtr.Zero);
            resizedImage.Save(dstPath);
        }

        public static async Task saveAndResizeImageAsync(Stream strm, int width, int height, string dstPath)
        {
            await Task.WhenAll(
                    Task.Run(() => {
                        Image image = System.Drawing.Image.FromStream(strm, true, true);
                        Image resizedImage = image.GetThumbnailImage(width, height, null, IntPtr.Zero);
                        resizedImage.Save(dstPath);
                    })
                );
        }

        public static async Task saveAndResizeImageAsync(string base64String, int width, int height, string dstPath)
        {

            var bytesBase64 = Convert.FromBase64String(base64String);
            var streamContent = new StreamContent(new MemoryStream(bytesBase64));

            ExtendUtil.saveAndResizeImage(await streamContent.ReadAsStreamAsync(), width, height, dstPath);
        }
    }
}

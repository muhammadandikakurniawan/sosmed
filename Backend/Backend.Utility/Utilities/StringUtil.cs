﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Backend.Utility.Utilities
{
    public static class StringUtil
    {

        public static string Hash(string plainTxt, HashAlgorithm algo)
        {
            StringBuilder sb = new StringBuilder();
            byte[] plainBytes = Encoding.UTF8.GetBytes(plainTxt);
            byte[] hashBytes = algo.ComputeHash(plainBytes);

            foreach(var bit in hashBytes)
            {
                sb.Append(String.Format("{0:x2}",bit));
            }

            return sb.ToString();
        }

    }
}

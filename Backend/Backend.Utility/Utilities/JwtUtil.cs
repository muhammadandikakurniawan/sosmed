﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Backend.Utility.Utilities
{
    public static class JwtUtil
    {
        public static string CreateToken(string signatureKey, string issuer, string audience, DateTime expire, List<Claim> claims)
        {
            //create token

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signatureKey));
            var credentialKey = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            var jwtSecurityToken = new JwtSecurityToken(
                    signingCredentials: credentialKey,
                    issuer: issuer,
                    audience: audience,
                    expires: expire,
                    claims: claims
                );

            var stringToken = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);

            return stringToken;
        }

        public static string CreateToken(string signatureKey, string issuer, string audience, DateTime expire, params Claim[] claims)
        {
            //create token

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signatureKey));
            var credentialKey = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            var jwtSecurityToken = new JwtSecurityToken(
                    signingCredentials: credentialKey,
                    issuer: issuer,
                    audience: audience,
                    expires: expire,
                    claims: claims
                );

            var stringToken = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);

            return stringToken;
        }

        public static int ValidateToken(string token, string signatureKey, string issuer, string audience)
        {
            int res = (int)validateJwtStatus.validToken;

            try
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signatureKey));

                var jwtSecurityHandler = new JwtSecurityTokenHandler();
                var tokenValidationParams = new TokenValidationParameters()
                {
                    IssuerSigningKey = securityKey,
                    ValidIssuer = issuer,
                    ValidAudience = audience,
                    ValidateIssuerSigningKey = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuer = true
                };
                var validateToken = jwtSecurityHandler.ValidateToken(token, tokenValidationParams, out var secureToken);

            }
            catch (SecurityTokenExpiredException ex)
            {
                res = (int)validateJwtStatus.expiredToken;
            }
            catch (SecurityTokenInvalidSignatureException ex)
            {
                res = (int)validateJwtStatus.invalidSignature;
            }
            catch (Exception ex)
            {
                res = (int)validateJwtStatus.invalidToken;
            }

            return res;
        }

        public static List<Claim> GetClaims(string token)
        {
            string secret = AppSettings.JwtSettings.SignatureKey;
            var key = Encoding.ASCII.GetBytes(secret);
            var handler = new JwtSecurityTokenHandler();
            var validations = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false
            };
            var claims = handler.ValidateToken(token, validations, out var tokenSecure);
            return claims.Claims.ToList();
        }

    }

    public enum validateJwtStatus
    {
        validToken = 1,
        invalidToken = 0,
        expiredToken = -1,
        invalidSignature = -2
    }
}


﻿using Backend.Service.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Api.AppStart
{
    public class ApiAuthenticationRequirement : IAuthorizationRequirement
    {
        private IAuthService _authService;
        public ApiAuthenticationRequirement(IAuthService authService)
        {
            _authService = authService;
        }
    }
}

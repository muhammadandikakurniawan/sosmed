using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Backend.ElasticSearchRepository.Repositories.Implements;
using Backend.ElasticSearchRepository.Repositories.Interfaces;
using Backend.PsgSQLRepository.Entities;
using Backend.PsgSQLRepository.Repositories.Implements;
using Backend.PsgSQLRepository.Repositories.Interfaces;
using Backend.Service.AppHubs;
using Backend.Service.Implements;
using Backend.Service.Interfaces;
using Backend.Utility.Utilities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;

namespace Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //======================================================= dependency injection ===========================================================
            //================= Service ========================
            services.AddTransient<IAuthService, AuthService>();
            services.AddTransient<IUserPostService, UserPostService>();
            //================= END Service ====================

            //================= Repository =====================
            services.AddDbContext<SosmedContext>(options => options.UseNpgsql(Configuration.GetValue<string>("ConnectionString:SosmedDb")),
                ServiceLifetime.Transient
             );
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IPoolUserAccessTokenRepository, PoolUserAccessTokenRepository>();
            services.AddTransient<IUserPostEsRepository, UserPostEsRepository>();

            //================= END Repository =================
            //======================================================= END dependency injection =======================================================



            services.AddMvc()
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null)
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddAuthentication(
                options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                }
            )
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, config => {
                //config.SaveToken = true;
                config.Events = new JwtBearerEvents()
                {
                    OnMessageReceived = (contex) =>
                    {
                        if (contex.Request.Headers.TryGetValue("Authorization", out var tokenAuth))
                        {
                            contex.Fail(new Exception("not authorize"));
                        }
                        if (contex.Request.Headers.TryGetValue("SosmedAuthorization", out var token))
                        {
                            contex.Token = token;
                        }
                        else
                        {
                            contex.Fail(new Exception("not authorize"));
                        }

                        return Task.CompletedTask;
                    },

                    OnTokenValidated = (contex) =>
                    {

                        return Task.CompletedTask;
                    },

                    OnAuthenticationFailed = (contex) =>
                    {

                        return Task.CompletedTask;
                    },
                };

                var jwtSecretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppSettings.JwtSettings.SignatureKey));

                config.TokenValidationParameters = new TokenValidationParameters()
                {
                    IssuerSigningKey = jwtSecretKey,
                    ValidateIssuerSigningKey = true,
                    ClockSkew = TimeSpan.Zero,
                    ValidateLifetime = true,
                    ValidIssuer = AppSettings.JwtSettings.Issuer.ToString(),
                    ValidateIssuer = true,
                    ValidAudience = AppSettings.JwtSettings.Audience.ToString(),
                    ValidateAudience = true,
                };

                }
            );


            services.AddCors(config => {
                config.AddDefaultPolicy(policy => {
                    policy.SetIsOriginAllowed(_ => true).AllowCredentials().AllowAnyHeader().AllowAnyMethod();
                });
            });

            services.AddHttpContextAccessor();
            
            services.AddControllersWithViews()
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddControllers()
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.Configure<FormOptions>(x => {
                x.MultipartHeadersLengthLimit = Int32.MaxValue;
                x.MultipartBoundaryLengthLimit = Int32.MaxValue;
                x.MultipartBodyLengthLimit = Int64.MaxValue;
                x.ValueLengthLimit = Int32.MaxValue;
                x.BufferBodyLengthLimit = Int64.MaxValue;
                x.MemoryBufferThreshold = Int32.MaxValue;
            });
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSignalR(routes => {
                    routes.MapHub<UserPostHub>("/UserPostHub");
                }
            );

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}

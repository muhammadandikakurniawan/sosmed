﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Backend.Object.Models;
using Backend.Object.Objects;
using Backend.Service.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Api.Controllers
{
    [Route("api/AuthService")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AuthController : ControllerBase
    {
        private IAuthService _authService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public AuthController(
            IAuthService authService
            , IHttpContextAccessor httpContextAccessor
            )
        {
            _authService = authService;
            _httpContextAccessor = httpContextAccessor;
        }


        [HttpPost]
        [Route("Login")]
        [AllowAnonymous()]
        public async Task<ServiceResultModel<LoginResultObject>> Login([FromBody] LoginParamObject param)
        {
            ServiceResultModel<LoginResultObject> result = await _authService.LoginAsync(param);

            return result;
        }

        [HttpPost]
        [Route("GetUserAuthorize")]
        public async Task<ServiceResultModel<DataUserObject>> GetUserAuthorize()
        {

            string token = "";

            if (_httpContextAccessor.HttpContext.Request.Headers.TryGetValue("SosmedAuthorization", out var SosmedAuthorization))
                token = SosmedAuthorization;

            ServiceResultModel <DataUserObject> result = await _authService.GetUserByAccessTokenAsync(token);

            return result;
        }

        [HttpPost]
        [Route("RefreshAccessToken")]
        public async Task<ServiceResultModel<DataUserObject>> RefreshAccessToken()
        {

            string token = "";

            if (_httpContextAccessor.HttpContext.Request.Headers.TryGetValue("SosmedAuthorization", out var SosmedAuthorization))
                token = SosmedAuthorization;

            ServiceResultModel<DataUserObject> result = await _authService.RefreshAccessTokenAsync(token);

            return result;
        }
    }
}

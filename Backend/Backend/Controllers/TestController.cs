﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.ElasticSearchRepository.Entities;
using Backend.Object.Models;
using Backend.Object.Objects;
using Backend.Service.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Api.Controllers
{
    [Route("api/Test")]
    [ApiController]
    public class TestController : ControllerBase
    {

        private IUserPostService _userPostService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public TestController(
            IUserPostService userPostService
            , IHttpContextAccessor httpContextAccessor
            )
        {
            _userPostService = userPostService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpPost, Route("AddUserPost")]
        public async Task<ServiceResultModel<UserPost>> AddUserPost([FromForm] AddPostParamObject param)
        {
            return await _userPostService.AddPost(param);
        }

        [HttpGet,Route("AppSettings")]
        public object AppSettings()
        {
            return Utility.Utilities.AppSettings.ElasticSerachSettings;
        }


    }
}

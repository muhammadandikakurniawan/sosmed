﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.ElasticSearchRepository.Entities;
using Backend.Object.Models;
using Backend.Object.Objects;
using Backend.Service.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Api.Controllers
{
    [Route("api/UserPostService")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserPostController : ControllerBase
    {
        private IUserPostService _userPostService;
        public UserPostController(IUserPostService userPostService)
        {
            _userPostService = userPostService;
        }

        [HttpPost, Route("AddPost")]
        public async Task<ServiceResultModel<UserPost>> AddPost([FromForm] AddPostParamObject param)
        {
            return await _userPostService.PushAddPost(param);
        }

        [HttpGet, Route("GetAll")]
        public async Task<DataPageResultModel<List<UserPost>>> GetAll(int from, int size)
        {
            return await _userPostService.GetAllPagerAsync(from, size);
        }

        [HttpGet, Route("GetByUserIDPager")]
        public async Task<DataPageResultModel<List<UserPost>>> GetByUserIDPager(string userId, int from, int size)
        {
            return await _userPostService.GetByUserIDPagerAsync(userId, from, size);
        }
    }
}

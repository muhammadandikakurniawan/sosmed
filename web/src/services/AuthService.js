import AuthServiceAdapter from '../adapters/AuthServiceAdapter';
import axios from 'axios';

export default class AuthService{

    constructor(){
        this.AuthAdapter = new AuthServiceAdapter();
    }

    async Login(param){

        var optionAdapter = {
            params : param,
            headers : {
                
            }
        }

        var optionHttp = this.AuthAdapter.Login(optionAdapter);
        axios.defaults.withCredentials = true;
        return await axios.post(optionHttp.url,optionHttp.params,optionHttp)

    }

    async GetUserAuthorize(param){

        var optionAdapter = {
            params : param,
            headers : {
                
            }
        }

        var optionHttp = this.AuthAdapter.GetUserAuthorize(optionAdapter);
        axios.defaults.withCredentials = true;
        return await axios.post(optionHttp.url,optionHttp.params,optionHttp)

    }

    async RefreshAccessToken(param){

        var optionAdapter = {
            params : param,
            headers : {
                
            }
        }

        var optionHttp = this.AuthAdapter.RefreshAccessToken(optionAdapter);
        axios.defaults.withCredentials = true;
        return await axios.post(optionHttp.url,optionHttp.params,optionHttp)

    }
    
    

}
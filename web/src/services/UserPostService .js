import UserPostServiceAdapter from '../adapters/UserPostServiceAdapter';
import axios from 'axios';

export default class AuthService{

    constructor(){
        this.UserPostServiceAdapter = new UserPostServiceAdapter();
    }

    async AddPost(formData){

        var adapter = this.UserPostServiceAdapter.AddPost();

        return await axios.post(adapter.url,formData,adapter.params);

    }

    async GetAll(param){

        var optionAdapter = {
            params : param,
            headers : {
                
            }
        }

        var optionHttp = this.UserPostServiceAdapter.GetAll(optionAdapter);
        axios.defaults.withCredentials = true;
        return await axios.get(optionHttp.url,
            {
                headers : optionHttp.headers,
                params : optionHttp.params,
                config : optionHttp
            });

    }
    
}
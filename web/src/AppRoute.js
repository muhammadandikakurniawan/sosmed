import React, { Component, Fragment } from 'react';
import { BrowserRouter, Switch, Route, Link, Redirect } from "react-router-dom";
import { Cookies } from 'react-cookie';
import AuthService from './services/AuthService';
import './vendors/fontawesome-free/css/all.min.css'

//================================ start import comonents ================================

import RegisterPage from './pages/RegisterPage';
import LoginPage from './pages/LoginPage';
import HomePage from './pages/HomePage';
//================================ end import comonents ================================

class AppRoute extends Component {

    render() {
        return (
            <Switch >

            <Route path = "/" exact render={() => (
                this.props.isLogin ? (<Redirect to="/Home"/>) : (<LoginPage/>)
            )}/>
            <Route path = "/Login" exact render={() => (
                this.props.isLogin ? (<Redirect to="/Home"/>) : (<LoginPage/>)
            )}/>
            <Route path = "/Register" exact render={() => (
                this.props.isLogin ? (<Redirect to="/Home"/>) : (<RegisterPage/>)
            )}/>
            <Route path = "/Home" exact render={() => (
                this.props.isLogin ? (<HomePage/>) : (<Redirect to="/Login"/>)
            )}/>
            
            </Switch>
        )

    }

}

export default AppRoute;
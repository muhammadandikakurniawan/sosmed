export default class AppSettings{
    
    static ApiHost = "https://localhost:44388"
    static UserPostFilePath = "http://localhost:8989/app_data/Sosmed/user_post_files/"
    static CookiesTokenName = "SosmedAccesToken"

}
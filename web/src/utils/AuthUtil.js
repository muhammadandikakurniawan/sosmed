import AuthService from '../services/AuthService'
import { Cookies } from 'react-cookie';

export default class AuthUtil{

    static getDataUserLogin = async () => {

        var authService = new AuthService();
        var cookies = new Cookies();

        var accessToken = cookies.get("SosmedAccesToken") || "";
        if(!accessToken){
            return {
                Email : "",
                Username : "",
                Id : ""
            }
        }else{
            var dataUser = await authService.GetUserAuthorize();

            if(dataUser.data.StatusCode == "00"){
                return {
                    Email : dataUser.data.Value.Email,
                    Username :dataUser.data.Value.Username,
                    Id : dataUser.data.Value.UserId
                }
            }
            
        }

        return {
            Email : "",
            Username : "",
            Id : ""
        }

    }

}
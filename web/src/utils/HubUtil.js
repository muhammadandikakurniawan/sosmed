import AuthService from '../services/AuthService'
import { Cookies } from 'react-cookie';
import * as signalR from '@aspnet/signalr';

export default class HubUtil{

    subcribe(method,callback){
        // create the connection instance
        const protocol = new signalR.JsonHubProtocol();

        const transport = signalR.HttpTransportType.WebSockets;

        const options = {
            transport,
            logMessageContent: true,
            logger: null,
        };
        this.connection = new signalR.HubConnectionBuilder()
        .withUrl("https://localhost:44301/UserPostHub", options)
        .withHubProtocol(protocol)
        .build();

        this.connection.start()
        .then((p) => {
            console.info('SignalR Connected'); console.log(p)
        })
        .catch(err =>{
        console.log('SignalR Connection Error: ', err)});

        
        // this.sendData();
        this.connection.on(method,(data)=>{
            callback(data)
        });
    }

    sendSignalR(method,msg){
        // create the connection instance
        const protocol = new signalR.JsonHubProtocol();

        const transport = signalR.HttpTransportType.WebSockets;

        const options = {
            transport,
            logMessageContent: true,
            logger: signalR.LogLevel.Trace,
        };
        this.connection = new signalR.HubConnectionBuilder()
        .withUrl("https://localhost:44301/UserPostHub", options)
        .withHubProtocol(protocol)
        .build();

        this.connection.start()
        .then(() => {
            console.info('SignalR Connected for send data'); 
            this.connection.invoke(method,msg).catch((e) => {
                alert(e)
            })
        })
        .catch(err => console.error('SignalR Connection Error: ', err));

    }

}
import AppSettings from '../utils/AppSettings'
import AuthUtil from '../utils/AuthUtil';
import { Cookies } from 'react-cookie';

export default class AuthServiceAdapter{

    constructor(){
        this.service = AppSettings.ApiHost+"/api/AuthService"
        this.cookies = new Cookies();
    }

    Login(param){
        param.headers["Content-Type"] = "application/json"
        param.headers["Accept"] = "application/json"
        return{
            url : this.service+"/Login/",
            params : param.params,
            headers : param.headers
        }
    }

    GetUserAuthorize(param){
        param.headers["SosmedAuthorization"] = this.cookies.get("SosmedAccesToken") || "";
        param.headers["Content-Type"] = "application/json"
        param.headers["Accept"] = "application/json"
        return{
            url : this.service+"/GetUserAuthorize",
            params : param.params,
            headers : param.headers
        }
    }

    RefreshAccessToken(param){
        param.headers["SosmedAuthorization"] = this.cookies.get("SosmedAccesToken") || "";
        param.headers["Content-Type"] = "application/json"
        param.headers["Accept"] = "application/json"
        return{
            url : this.service+"/RefreshAccessToken",
            params : param.params,
            headers : param.headers
        }
    }
    
}
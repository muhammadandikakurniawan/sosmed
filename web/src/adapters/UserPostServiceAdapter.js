import AppSettings from '../utils/AppSettings'
import AuthUtil from '../utils/AuthUtil';
import { Cookies } from 'react-cookie';

export default class AuthServiceAdapter{

    constructor(){
        this.service = AppSettings.ApiHost+"/api/UserPostService"
        this.cookies = new Cookies();
        this.authHeader = this.cookies.get("SosmedAccesToken") || "";
    }

    AddPost(){
        return{
            url : this.service+"/AddPost",
            params : {
                headers: {
                    "content-type": "multipart/form-data",
                    "SosmedAuthorization" : this.authHeader
                }
            }
        }
    }
    
    GetAll(param){
        param.headers["SosmedAuthorization"] = this.cookies.get("SosmedAccesToken") || "";
        param.headers["Content-Type"] = "application/json"
        param.headers["Accept"] = "application/json"
        return{
            url : this.service+"/GetAll",
            params : param.params,
            headers : param.headers
        }
    }
}
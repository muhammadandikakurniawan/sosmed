import React from 'react';
import { Alert, Card, Button, Container, Row, Form, Image, Col, Spinner, Modal, Jumbotron } from 'react-bootstrap';
import AuthService from '../services/AuthService';

export default class LoginPage extends React.Component{

    constructor(props){
        super(props)
        this.authService = new AuthService()
        this.state = {
            showAlert : false,
            alertTitle : "",
            alertVariant : "success",
            dataLogin : {
                Email : "",
                Password : ""
            }
        }
    }

    async onChangedataLogin(type, val){
        switch(type){
            case "Email":
                await this.setState(prevState => ({
                    dataLogin : {
                        ...prevState.dataLogin,
                        Email : val
                    }
                }))
                break;
            case "Password":
                await this.setState(prevState => ({
                    dataLogin : {
                        ...prevState.dataLogin,
                        Password : val
                    }
                }))
                break;
        }
    }

    login = async () => {
        var loginRes = await this.authService.Login(this.state.dataLogin)
        if(loginRes.data.StatusCode == "00"){
            await this.setState(prevState => ({
                dataLogin : {
                    ...prevState.dataLogin,
                    Email : "",
                    Password : ""
                }
            }))
            window.location.href = "/Home"
        }
        else if(loginRes.data.StatusCode == "500"){
            this.setState({showAlert:true,alertVariant:"danger",alertTitle:"Terjadi kesalahan, mohon coba lagi nanti"})
        }
        else{
            this.setState({showAlert:true,alertVariant:"danger",alertTitle:"Login gagal, mohon periksa data diri anda, atau coba lagi nanti, "+ loginRes.data.Message})
        }
        var dbg = ""
    }

    setAlertShow = async (param) => {
        await this.setState({showAlert:param})
    }

    render(){

        return(
            <div class="container">

            <Alert variant={this.state.alertVariant} onClose={() => this.setAlertShow(false)} hidden={!this.state.showAlert} dismissible>
            <Alert.Heading>{this.state.alertTitle}</Alert.Heading>
            </Alert>

            <center>
                
            <div class="card o-hidden border-0 shadow-lg my-5 col-lg-6 center">
                <div class="card-body p-0 center">
                    <div class="row center">
                    <center>
                    <div class="col-lg-12 row">
                        <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Login</h1>
                        </div>
                        <form class="user col-lg-12">
                            <div class="form-group row">
                            <div class="col-sm-12 mb-3 mb-sm-0">
                                <Form.Control type="email" placeholder="Email" className="my-1 form-control form-control-user" value={this.state.dataLogin.Email} onChange={(e) => {this.onChangedataLogin("Email",e.target.value)}}/>
                            </div>
                            <div class="col-sm-12">
                                <Form.Control type="password" placeholder="Password" className="my-3 form-control form-control-user" value={this.state.dataLogin.Password} onChange={(e) => {this.onChangedataLogin("Password",e.target.value)}}/>
                            </div>
                            </div>
                            
                            <Button className="btn btn-primary btn-user btn-block" onClick={this.login}>Login</Button>
                            <hr></hr>
                            <div class="text-center">
                                <a class="small" href="/Register">Register ?</a>
                            </div>
                        </form>
                        </div>
                    </div>
                    </center>
                    </div>
                </div>
            
            </div>
    
            </center>
          </div>
        
        )
    }
}
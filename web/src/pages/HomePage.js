import React from 'react';
import { ListGroup, Carousel, Button, Container, Row, Form, Alert, Col, Card, Modal, FormControl,Dropdown } from 'react-bootstrap';
import AuthService from '../services/AuthService';
import UserPostService from '../services/UserPostService ';
import AuthUtil from '../utils/AuthUtil';
import HubUtil from '../utils/HubUtil';
import AppSettings from '../utils/AppSettings';

export default class HomePage extends React.Component{

    constructor(props){
        super(props)
        this.authService = new AuthService();
        this.userPostService = new UserPostService();
        this.hubUtil = new HubUtil();
        this.state = {
            dataNewPost : [

            ],
            dataPosts:[

            ],
            dataUser : {
                Email : "",
                Username : "",
                Id : ""
            },
            showAddPostModal:false,
            showNewPostNotif : false,
            dataAddPost:{
                Files:[],
                Description:"",
                Contents : []
            },
        }
        
        this.setDataUserLogin().then((e) => {
            this.setPosts();
        });
        
    }

    componentDidMount = async () =>{
        this.hubUtil.subcribe("PushNotifAddPost", (res) => {
            console.log("======================= realtime notif ========================");
            console.log(res);
            var datanewPost = {
                "CreatedDate": res.value.createdDate,
                "Description": res.value.description,
                "Id": res.value.id,
                "IsDeleted": res.value.isDeleted,
                "Title": res.value.title,
                "UseId": res.value.useId,
            }
            console.log(datanewPost);
            if(res.value.files){
                datanewPost["Files"] = res.value.files.map(f => {
                    return {
                        Extension: f.extension,
                        FileName: f.fileName,
                        IsImage: f.isImage,
                        IsVideo: f.isVideo,
                        Size: f.size
                    }
                })
            }
            var newPost = this.state.dataNewPost;
            newPost.unshift(datanewPost);
            this.setState((prev) => ({
                showNewPostNotif: true,
                dataNewPost : newPost
            }))
        })
    }

    setPosts = async () => {

        var dataPost = await this.userPostService.GetAll({"from":0,"size":5});
        console.log(dataPost);
        await this.setState((prev) => ({
            dataPosts : dataPost.data.Data
        }));

    }

    handleAddPostModal = (isShow) => {
        this.setState({showAddPostModal:isShow});
    }

    setDataUserLogin = async () => {

        var res = await AuthUtil.getDataUserLogin();

        this.setState(prev => ({
            dataUser : res
        }))
    }

    openFileBrowser = (id) => {
        document.querySelector("#"+id).click()
    }

    async onChangeDataPost(type, val){
        switch(type){
            case "Files":
                console.log(val)
                await this.setState(prevState => ({
                    dataAddPost : {
                        ...prevState.dataAddPost,
                        Files : val
                    }
                }))
                break;

            case "Images":
                console.log(val)
                await this.setDataPreviewImageVideo(val);
                await this.setState(prevState => ({
                    dataAddPost : {
                        ...prevState.dataAddPost,
                        Files : val
                    }
                }));
                break;

            case "Description":
                await this.setState(prevState => ({
                    dataAddPost : {
                        ...prevState.dataAddPost,
                        Description : val
                    }
                }))
                break;
        }
    }

    setDataPreviewImageVideo = async (files) => {
        
        var regImage = new RegExp("^(data:image/).*");
        var that = this;
        for(var i = 0; i < files.length; i++){
            var file = files[i]
            if(file){
                var reader = new FileReader();

                reader.onload = function(e){
                    var result = e.target.result;
                    if(regImage.exec(result)){

                    }
                    var contents = that.state.dataAddPost.Contents;
                    contents.push(result);
                    that.setState(prevState => ({
                        dataAddPost : {
                            ...prevState.dataAddPost,
                            Contents : contents
                        }
                    }));
                }
        
                reader.readAsDataURL(file)
            }
        }
    }

    resizeImage = () => {
        var contents = this.state.dataAddPost.Contents;
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext("2d");
        var img = new Image();

        this.state.dataAddPost.Contents.forEach((imgUrl,i) => {
            img.onload = function () {

                // set size proportional to image
                canvas.height = canvas.width * (img.height / img.width);
    
                // step 1 - resize to 50%
                var oc = document.createElement('canvas'),
                    octx = oc.getContext('2d');
    
                oc.width = img.width * 0.5;
                oc.height = img.height * 0.5;
                octx.drawImage(img, 0, 0, oc.width, oc.height);
    
                // step 2
                octx.drawImage(oc, 0, 0, oc.width * 0.5, oc.height * 0.5);
    
                // step 3, resize to final size
                ctx.drawImage(oc, 0, 0, oc.width * 0.5, oc.height * 0.5,
                0, 0, canvas.width, canvas.height);
                var result = canvas.toDataURL("image/jpeg");
                contents.push(result);
            }
            img.src = imgUrl;
        });
    }

    addPost = async () => {
        console.log(this.state.dataAddPost);
        console.log(this.state.dataUser.UserId);

        var formData = new FormData();
        formData.append("UserId", this.state.dataUser.Id);
        formData.append("Description", this.state.dataAddPost.Description);
        for(var i = 0; i < this.state.dataAddPost.Files.length; i++){
            formData.append("Files", this.state.dataAddPost.Files[i]);
        }

        var addPostRes = await this.userPostService.AddPost(formData);

        console.log("========================== add post result =========================")
        console.log(addPostRes)
    }

    closeNewPostAlert = () => {
        this.setState({showNewPostNotif : false});
        var dataPosts = this.state.dataPosts;
        this.state.dataNewPost.forEach(e => {
            dataPosts.unshift(e);
        });
        
        this.setState((prev) => ({
            dataPosts : dataPosts,
            dataNewPost : []
        }));
    }

    renderCrouselAddPostImage = () => {
        var regImage = new RegExp("^(data:image/).*");
        var regVideo = new RegExp("^(data:video/).*");
        if(this.state.dataAddPost.Contents != null && this.state.dataAddPost.Contents.length > 0){
            return (
                <Carousel>
                    {
                        this.state.dataAddPost.Contents.map((e,i) => {
                            if(regImage.exec(e)){
                                return(
                                    <Carousel.Item>
                                        <img
                                            id={"contentImg#"+i}
                                            width="80%" height="25%"
                                            className="d-block w-100"
                                            src={e}
                                        />
                                    </Carousel.Item>
                                )
                            }
                            if(regVideo.exec(e)){
                                return(
                                    <Carousel.Item>
                                        <video src={e}  width="80%" height="25%" controls controlsList="nodownload"></video>
                                    </Carousel.Item>
                                )
                            }
                        })
                    }
                    
                </Carousel>
            )
        }else{
            return null;
        }
        
    }

    renderPosts = () => {

        return (
            <Row className="justify-content-md-center"  style={{ width: '100%'}}>
                <Col sm={12} style={{}}>

                    {this.state.dataPosts.map((e, i) => {

                        return(
                            <Container style={{width: '100%'}}>
                                <Card style={{width: '70%', height:"auto",backgroundColor:"#062c52", color:"white", border:"0px solid white"}} className="mt-5">
                                    <Carousel>
                                        {
                                            e.Files.map((f, fi) => {
                                                if (f.IsImage) {
                                                    return (
                                                        <Carousel.Item>
                                                            <img
                                                                id={"contentImg#" + i}
                                                                className="d-block w-100"
                                                                src={AppSettings.UserPostFilePath+f.FileName}
                                                            />
                                                        </Carousel.Item>
                                                    )
                                                }
                                                if (f.IsVideo) {
                                                    return (
                                                        <Carousel.Item>
                                                            <video src={AppSettings.UserPostFilePath+f.FileName} width="600px" height="400px" controls controlsList="nodownload"></video>
                                                        </Carousel.Item>
                                                    )
                                                }
                                            })
                                        }

                                    </Carousel>
                                    <Card.Body style={{}}>
                                        <Card.Text style={{ width: '100%'}} >
                                            {e.Description}
                                        </Card.Text>
                                        <Row>
                                            <Col style={{fontSize:"25px",margin:"50 50 50 50"}}>
                                            <a href="#" style={{}}><i class="far fa-comment"></i></a>
                                            <a href="#" style={{margin:"50px"}}><i class="far fa-thumbs-up"></i></a>
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                    
                                </Card>
                                <hr color="white" />
                            </Container>
                        );

                    })}
                </Col>
                
            </Row>
        )

    }

    render(){
        return(
            <Container className="" fluid style={{backgroundColor:"#062c52"}}>
                <Row className="justify-content-md-center" style={{backgroundColor:"#062c52"}}>
                    <Col sm={3} className="justify-content-md-center mt-3"  style={{backgroundColor:"#062c52"}}>
                        <ListGroup variant="flush"  style={{backgroundColor:"#062c52", position:"fixed", width:"20%"}}>
                            <ListGroup.Item style={{backgroundColor:"#062c52", color:"#4580ff", borderBottom:"1px solid #4580ff", fontSize:"125%", fontWeight:"bold"}}><a href="/Home" style={{textDecoration:"none"}}><i class="fas fa-home"></i> Branda</a></ListGroup.Item>
                            <ListGroup.Item style={{backgroundColor:"#062c52", color:"#4580ff", borderBottom:"1px solid #4580ff", fontSize:"125%", fontWeight:"bold"}}><a href="#" style={{textDecoration:"none"}}><i class="fas fa-user-circle"></i> Profile</a></ListGroup.Item>
                            <ListGroup.Item style={{backgroundColor:"#062c52", color:"#4580ff", borderBottom:"1px solid #4580ff", fontSize:"125%", fontWeight:"bold"}}><a href="#" style={{textDecoration:"none"}}><i class="fas fa-compass"></i> Explore</a></ListGroup.Item>
                            <ListGroup.Item style={{backgroundColor:"#062c52", color:"#4580ff", borderBottom:"1px solid #4580ff", fontSize:"125%", fontWeight:"bold"}}><a href="#" style={{textDecoration:"none"}}><i class="fas fa-bell"></i> Notification</a></ListGroup.Item>
                            <hr/>
                            <Button style={{color:"#062c52", fontWeight:"bold"}} variant="primary" onClick={() => {this.handleAddPostModal(true)}}>Add Post</Button>
                        </ListGroup>
                    </Col>
                    <Col sm={6} className="justify-content-md-center mt-3" >
                    <ListGroup variant="flush" style={{backgroundColor:"#062c52",}}>
                        <ListGroup.Item style={{width:"100%", height:"200px",backgroundColor:"#295480", color:"white", border:"0px solid #4580ff"}}>{this.state.dataUser.Username}</ListGroup.Item>
                        <ListGroup.Item style={{ backgroundColor:"#062c52", alignContent:"center", border:"0px solid white"}} className="justify-content-md-center">
                            <Alert style={{}} key="NotifNewPost" variant="primary" show={this.state.showNewPostNotif} onClose={() => { this.closeNewPostAlert()}} dismissible>
                                New Post
                            </Alert>
                            <center>
                            {this.renderPosts()}
                            </center>
                        </ListGroup.Item>
                    </ListGroup>
                    </Col>
                    <Col sm={3} className="justify-content-md-center mt-3" style={{width:"100%", height:"200%", position:"relatve",top:0,right:0}}>
                        <Container style={{width:"20%", height:"100%", position:"fixed"}}>
                            <Form>
                                <FormControl style={{width:"100%", height:"50px",backgroundColor:"#295480", color:"white", border:"0px solid #4580ff", display:"inline-block"}} type="text" placeholder="Search" className="mr-sm-2" />
                            </Form>
                            <Dropdown style={{width:"20%", display:"inline-block", position:"fixed", top:"95%", left:"0",backgroundColor:"#295480"}}>
                                <Dropdown.Toggle id="dropdown-basic" style={{width:"100%", display:"inline-block",backgroundColor:"#295480"}}>
                                    Direct Message
                                </Dropdown.Toggle>

                                <Dropdown.Menu style={{width:"500%", height:"100%", display:"inline-block",backgroundColor:"#295480"}}>
                                    <Container>
                                        <ListGroup style={{width:"200%", backgroundColor:"#295480"}}>
                                            <ListGroup.Item>Cras justo odio</ListGroup.Item>
                                        </ListGroup>
                                    </Container>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Container>
                    </Col>
                </Row>

                <Modal show={this.state.showAddPostModal} onHide={() => {this.handleAddPostModal(false)}}>
                    <Modal.Header style={{backgroundColor:'#062c52'}} closeButton>
                    </Modal.Header>
                    <Modal.Body style={{backgroundColor:'#062c52'}}>
                    {this.renderCrouselAddPostImage()}
                    <Form.Group controlId="exampleForm.ControlTextarea1">
                        <Form.Label style={{color:'white'}}>What happend ?</Form.Label>
                        <Form.Control style={{
                        backgroundColor:'#062c52',
                        borderColor: '#062c52',
                        color:'white',
                        webkitBoxShadow: 'none',
                        boxShadow: 'none',
                        resize:'none'
                        }} res as="textarea" rows={15} onChange={(e) => {this.onChangeDataPost("Description", e.target.value)}}/>
                    </Form.Group>
                    </Modal.Body>
                    <Modal.Footer style={{backgroundColor:'#062c52', width:'100%'}}>
                        <Row style={{backgroundColor:'#062c52', width:'100%'}}>
                            <Col sm={10}>
                                <Button style={{backgroundColor:'#062c52', border:'0'}} variant="primary" size="lg" onClick={() => {this.openFileBrowser("ImageVideBrowser")}}>
                                <i class="far fa-image"></i>
                                </Button>
                                <Button style={{backgroundColor:'#062c52', border:'0'}} variant="primary" size="lg" onClick={() => {this.openFileBrowser("FileBrowser")}}>
                                <i class="fas fa-paperclip"></i>
                                </Button>
                                <Form.Group controlId="exampleForm.Image">
                                    <Form.File hidden={true} id="ImageVideBrowser" accept="image/*,video/*" onChange={(e) => { this.onChangeDataPost("Images",e.target.files)}} multiple={true}/>
                                </Form.Group>
                                    <Form.Group controlId="exampleForm.File">
                                <Form.File hidden={true} id="FileBrowser" accept="application/x-zip-compressed" onChange={(e) => { this.onChangeDataPost("Files",e.target.files)}} multiple={true}/>
                            </Form.Group>
                            </Col>
                            <Col sm={2}>
                                <Button variant="primary" size="md" onClick={this.addPost}>
                                    Post
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Footer>
                </Modal>
            </Container>
        )
    }
}
import React from 'react';
import { Alert, Card, Button, Container, Row, Form, Image, Col, Spinner, Modal, Jumbotron } from 'react-bootstrap';
import AuthService from '../services/AuthService';

export default class RegisterPage extends React.Component{

    constructor(props){
        super(props)
        this.authService = new AuthService()
        this.state = {
            showAlert : false,
            alertTitle : "Registrasi berhasil, mohon check email anda untuk verifikasi akun",
            alertVariant : "success",
            dataRegister : {
                Username : "",
                Email : "",
                Password : "",
                ConfirmPassword : ""
            }
        }
    }

    async onChangeDataRegister(type, val){
        switch(type){
            case "Username":
                await this.setState(prevState => ({
                    dataRegister : {
                        ...prevState.dataRegister,
                        Username : val
                    }
                }))
                break;
            case "Email":
                await this.setState(prevState => ({
                    dataRegister : {
                        ...prevState.dataRegister,
                        Email : val
                    }
                }))
                break;
            case "Password":
                await this.setState(prevState => ({
                    dataRegister : {
                        ...prevState.dataRegister,
                        Password : val
                    }
                }))
                break;
            case "ConfirmPassword":
                await this.setState(prevState => ({
                    dataRegister : {
                        ...prevState.dataRegister,
                        ConfirmPassword : val
                    }
                }))
                break;
        }
    }

    register = async () => {
        var registerRes = await this.authService.Register(this.state.dataRegister)
        if(registerRes.data.StatusCode == "00"){
            await this.setState(prevState => ({
                dataRegister : {
                    ...prevState.dataRegister,
                    Username : "",
                    Email : "",
                    Password : "",
                    ConfirmPassword : ""
                }
            }))
            this.setState({showAlert:true,alertVariant:"success", alertTitle:"Registrasi berhasil, mohon check email anda untuk verifikasi akun"})
        }
        else if(registerRes.data.StatusCode == "500"){
            this.setState({showAlert:true,alertVariant:"danger",alertTitle:"Terjadi kesalahan, mohon coba lagi nanti"})
        }
        else{
            this.setState({showAlert:true,alertVariant:"danger",alertTitle:"Registrasi gagal, mohon periksa data diri anda, atau coba lagi nanti, "+ registerRes.data.Message})
        }
        var dbg = ""
    }

    setAlertShow = async (param) => {
        await this.setState({showAlert:param})
    }

    render(){

        return(
            <div class="container">

            <Alert variant={this.state.alertVariant} onClose={() => this.setAlertShow(false)} hidden={!this.state.showAlert} dismissible>
            <Alert.Heading>{this.state.alertTitle}</Alert.Heading>
            </Alert>

            <center>
                
            <div class="card o-hidden border-0 shadow-lg my-5 col-lg-6 center">
                <div class="card-body p-0 center">
                    <div class="row center">
                    <center>
                    <div class="col-lg-12 row">
                        <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                        </div>
                        <form class="user col-lg-12">
                            <div class="form-group row">
                            <div class="col-sm-12 mb-3 mb-sm-0">
                                <Form.Control type="text" placeholder="User Name" className="form-control form-control-user" value={this.state.dataRegister.Username} onChange={(e) => {this.onChangeDataRegister("Username",e.target.value)}}/>
                            </div>
                            </div>
                            <div class="form-group">
                            <Form.Control type="email" placeholder="Email Address" className="form-control form-control-user" value={this.state.dataRegister.Email} onChange={(e) => {this.onChangeDataRegister("Email",e.target.value)}}/>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <Form.Control type="password" placeholder="Password" className="form-control form-control-user" value={this.state.dataRegister.Password} onChange={(e) => {this.onChangeDataRegister("Password",e.target.value)}}/>
                            </div>
                            <div class="col-sm-6">
                                <Form.Control type="password" placeholder="Repeat Password" className="form-control form-control-user" value={this.state.dataRegister.ConfirmPassword} onChange={(e) => {this.onChangeDataRegister("ConfirmPassword",e.target.value)}}/>
                            </div>
                            </div>
                            
                            <Button className="btn btn-primary btn-user btn-block" onClick={this.register}>Register Account</Button>

                        </form>
                        <hr></hr>
                        <div class="text-center">
                            <a class="small" href="/Login">Already have an account? Login!</a>
                        </div>
                        </div>
                    </div>
                    </center>
                    </div>
                </div>
            
            </div>
    
            </center>
          </div>
        
        )
    }
}
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Switch, Route, Link, Redirect } from "react-router-dom";
import { Cookies } from 'react-cookie';
import AuthService from './services/AuthService';
import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import AppRoute from './AppRoute';
import $ from "jquery";
import "bootstrap/dist/js/bootstrap.bundle.min";


class App extends React.Component{

  constructor(props){
    super(props)
    this.cookies = new Cookies();
    this.authService = new AuthService();
    this.state = {
      isLogin : false
    }
  }

  componentDidMount(){
    this.isLogin();
  }

  isLogin = async () => {
    var result = false;

    var accessToken = this.cookies.get("SosmedAccesToken") || "";

    if(accessToken){
      var getDataUserByToken = await this.authService.GetUserAuthorize();
      console.log(getDataUserByToken);
      result = getDataUserByToken.data.StatusCode == "00";
      if(getDataUserByToken.data.StatusCode == "14"){
        var refreshTokenRes = await this.authService.RefreshAccessToken();
        result = refreshTokenRes.data.StatusCode == "00";
        console.log(refreshTokenRes);
      }
    }

    this.setState({isLogin : result});

    return result;

  }

  render(){
    console.log(this.state.isLogin)
    return (
        <BrowserRouter>
          <AppRoute isLogin={this.state.isLogin}/> 
        </BrowserRouter>
    );
  }

}

export default App;
